import os

raw_dir = "../raw/"
raw_filelist = os.listdir(raw_dir)

def unique_words():
        for filename in raw_filelist:
            with open(raw_dir+filename, 'r') as filetext:
                filetext = filetext.read()
                filetext = filetext.replace("\n"," ")

                words = filetext.split(' ')
                words = [word for word in words if word!=""]
                
                unique_words = set(words)
                unique_words_text = "\n".join(list(unique_words))
                with open("unique words/"+filename+"_UNIQUE_WORDS.txt",'w') as unique_word_file:
                    unique_word_file.write(unique_words_text)
                    

if __name__=='__main__':
    unique_words()
