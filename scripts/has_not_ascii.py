from unique_words import unique_words
import os
import re

#unique_words()
unique_words_dir = "unique words/"
unique_words_filelist = os.listdir(unique_words_dir)
es_regex = re.compile(r".?[á-úÁ-ÚA-Za-z]+.?", re.UNICODE)

for uw_filename in unique_words_filelist:

    with open(unique_words_dir+uw_filename,'r') as filename:
        words = filename.read().split('\n')
        words = [word for word in words if word != ""]
        
        non_es_words = [word for word in words if es_regex.match(word)==None]
        print(non_es_words)
    break
