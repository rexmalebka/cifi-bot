let totallength = 500;

function restrictdot(word){
	word = word.toLowerCase();
	const restrictions="el, él, la, los, las un, unos, unas, a, ante, bajo, cabe, con, contra, de, desde, durante, en, entre, hacia, hasta, mediante, para, por, según, sin, so, sobre, tras, versus, vía, Y, e, ni, que, o, cuando, al, mientras, donde, como, muy, me, te, se, nos, le, os, hi, pero, aunque, excepto, salvo, menos";
	let rules = restrictions.split(", ")

	if(rules.includes(word)){
		return true
	}else{
		return false
	}

}

function occurrences(string, subString, allowOverlapping) {
    string += "";
    subString += "";
    if (subString.length <= 0) return (string.length + 1);
    var n = 0,
        pos = 0,
        step = allowOverlapping ? 1 : subString.length;
    while (true) {
        pos = string.indexOf(subString, pos);
        if (pos >= 0) {
            ++n;
            pos += step;
        } else break;
    }
    return n;
}


const resultbox = document.querySelector("#result");
const seedinput = document.querySelector("#seed");
const lengthinput = document.querySelector("#totallength");
const genbutton = document.querySelector("#genbutton");

genbutton.addEventListener("click", function(e){
	resultbox.innerHTML = "";
	totallength = (lengthinput.value == "" || isNaN(lengthinput.value))? 100 : lengthinput.value;
	generate(seedinput.value);
});

function generate(seed){
	//just for counting 

	let words = Object.keys(metrics); // total words	
	let text = seed;
	let corpus = seed.split(" ")[seed.split(" ").length-1];

	// if corpus not found generates another randomly (for now) 
	while(metrics[corpus]==undefined && corpus!='.'){
		corpus = words[Math.floor(Math.random()*words.length)]; //random corpus
	}

	let ref = Object.entries(metrics[corpus]);
	let numwords = ref[0][0].split(" ").length;

	const iter = Math.floor(totallength / numwords) + ((totallength % numwords)>0 ? 1 : 0);


	//const met = [metrics, metrics3, metrics4, metrics5, metrics7, metrics8, metrics9, metrics10, metrics11];
	
	const met = [metrics];
	met.forEach(function(metrics, nn){
		if(nn>0){
			words = Object.keys(metrics); // total words
			corpus = words[Math.floor(Math.random()*words.length)]; //random corpus
			ref = Object.entries(metrics[corpus]);
		}
		
		for(let k=0;k<iter/met.length;k++){
			if(ref==undefined || ref.length==1 || (ref.length==1 && restrictdot(ref[0]))){
				// if there is no continuation, chooose random word
				text += " .|";
				while(ref.length==1){
					ref = words[Math.floor(Math.random()*words.length)];
					ref = Object.entries(metrics[ref]);
				}
			}

			// sort words acording to probability
			ref.sort((a,b)=> (b[1] > a[1]) ? 1 : -1  );

			// chooses best word
			console.log("BEST",ref)
			best = ref[(ref.length>3? Math.floor(Math.random()*3): 0 )][0]

			// restrict words before dot
			while(restrictdot(best)){
				ref = words[Math.floor(Math.random()*words.length)];
				ref = Object.entries(metrics[ref]);
				ref.sort((a,b)=> (b[1] > a[1]) ? 1 : -1  );
				//best = ref[0][0]
				best = ref[(ref.length>3? Math.floor(Math.random()*3): 0 )][0]
			}
			
			let tries = 0;
			while(occurrences(text, best,true)>0){
				if(tries==2){
					ref = Object.entries(metrics[words[Math.floor(Math.random()*words.length)]]);
					best = ref[0][0];

					tries = 0;
				}else{
					best = ref[(ref.length>=3) ? Math.floor(Math.random()*3): 0 ][0];
					tries ++;
				}

			}


			// if multiple words, continues with last word
			text += " "+best;
			ref = (best.split(" ").length > 1) ? best.split(" ")[best.split(" ").length-1] : best;
			let opt = metrics[ref];
			ref = Object.entries(opt)
		}
	});
	/*
	for(let k=0;k<iter;k++){
		if(ref==undefined || ref.length==1){
			// if there is no continuation, chooose random word
			text += " .|";
			while(ref.length==1){
				ref = words[Math.floor(Math.random()*words.length)];
				ref = Object.entries(metrics[ref]);
			}
		}
		// sort words acording to probability
		ref.sort((a,b)=> (b[1] > a[1]) ? 1 : -1  );

		// chooses best word
		best = ref[0][0]
		
		let tries = 0;
		while(occurrences(text, best,true)>0){
			if(tries==2){
				ref = Object.entries(metrics[words[Math.floor(Math.random()*words.length)]]);
				best = ref[0][0];

				tries = 0;
			}else{
				best = ref[(ref.length>=3) ? Math.floor(Math.random()*3): 0 ][0];
				tries ++;
			}

		}


		// if multiple words, continues with last word
		text += " "+best;
		ref = (best.split(" ").length > 1) ? best.split(" ")[best.split(" ").length-1] : best;
		let opt = metrics[ref];
		ref = Object.entries(opt)
	}
	*/

	// cut paragraphs
	//
	let textcut = text.replace(/ .\|/g,"@-@")
	console.log(textcut,"*")
	textcut = textcut.split(".")
	textcutsplit = []
	textres = ""
	for(let k=0;k<=textcut.length/4;k++){
		console.log(k*4,k*4 +4)
		textres += textcut.slice(k*4, k*4 + 4).join(".")+"@-@"
	}
	text = textres.replace(/\@-\@/g," .|")

	textsplit = text.split(" .|").slice(0,4)
	textsplit = textsplit.filter( (part) => part!="")
	text = textsplit.join(".\n")
	text = text.split(" ").slice(0,totallength).join(" ");

	text.split("\n").forEach(function(paragraph){
		let p = document.createElement("p");
		paragraph = paragraph.startsWith(" ") ? paragraph.slice(1) : paragraph
		paragraph = paragraph[0].toUpperCase() + paragraph.slice(1);
		p.textContent = paragraph;
		resultbox.appendChild(p)
	})
}
