from os import listdir
from os.path import isfile
from collections import defaultdict, Counter
from get_metrics import get_metrics
from export_json import export_json
from moreweight import moreweight

raw_dir = "../raw/"

raw_filelist = listdir(raw_dir)
raw_filelist = [filename for filename in raw_filelist if isfile(raw_dir+filename)]
def markov():

    metrics = defaultdict(Counter)

    for filename in raw_filelist:
        metrics = get_metrics(raw_dir+filename)

    metrics = moreweight(metrics)
    export_json(metrics)
if __name__ == "__main__":
    markov()
