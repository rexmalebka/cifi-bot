def normalize(words):
    return words

def normalize2(words):
    new_words = []
    for word in words:
        if word.startswith("¿"):
            new_words.extend(["¿",word[1:]])
            continue
        if word.startswith("_"):
            new_words.extend(["_",word[1:]])
            continue
        if word.startswith("-"):
            new_words.extend(["-",word[1:]])
            continue
        if word.startswith("["):
            new_words.extend([word[1:]])
            continue
        if word.startswith("—"):
            new_words.extend(["[",word[1:]])
            continue

        if word.endswith("]"):
            new_words.extend([word[:-1]])
            continue
        if word.endswith(".\n"):
            new_words.extend([word[:-1],".|"])
            continue
        if word.endswith("."):
            new_words.extend([word[:-1],"."])
            continue
        if word.endswith(","):
            new_words.extend([word[:-1],","])
            continue
        if word.endswith("_"):
            new_words.extend([word[:-1],"_"])
            continue
        if word.endswith("?"):
            new_words.extend([word[:-1],"?"])
            continue
        new_words.append(word)
    return new_words

