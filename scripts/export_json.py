import json

def export_json(metrics, filename="metrics.json"):
    
    json_metrics = json.dumps(metrics, ensure_ascii=False)
    
    with open(filename,"w", encoding="utf8") as json_file:
        json.dump(json_metrics, json_file, ensure_ascii=False)
        
    with open("static/js/metrics.js","w", encoding="utf8") as js_file:
        js_file.write("const metrics = ")
        js_file.write(str(json_metrics))
    return json_metrics
